# Mouad-Vim : Steve Francia's Vim Distribution ([spf13-vim]) Clone.

                    __ _ _____              _
         ___ _ __  / _/ |___ /      __   __(_)_ __ ___
        / __| '_ \| |_| | |_ \ _____\ \ / /| | '_ ` _ \
        \__ \ |_) |  _| |___) |_____|\ V / | | | | | | |
        |___/ .__/|_| |_|____/        \_/  |_|_| |_| |_|
            |_|

## HOW TO Install:

`curl https://bitbucket.org/mouad/mouad-vim/downloads/bootstrap.sh -L -o - | sh`

## Plugins Added:

* [vim-JSLint]
* [vim-ZenCoding]

## Small bugs fixed:

* Install [vim-ack] by detecting the presence of ack-grep command instead of the command ack (Usefull for Ubuntu machines).
* Fix the [vim-CSApprox] color detection following the suggestion [here](http://www.alfredrossi.com/?p=49)


[spf13-vim]: https://github.com/spf13/spf13-vim
[vim-JSLint]: https://github.com/hallettj/jslint.vim
[vim-ZenCoding]: http://mattn.github.com/zencoding-vim/
[vim-ack]: https://github.com/jtaby/VimAck
[vim-CSApprox]: https://github.com/vim-scripts/CSApprox
